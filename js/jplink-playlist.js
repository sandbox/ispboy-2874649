(function ($, Drupal) {
  Drupal.behaviors.jplinkStream = {
    attach: function (context, settings) {
      jplink = settings.jplink;
      var myPlaylist = new Array();
      for (var player_id in jplink) {
        load(player_id, jplink[player_id]);
      }


      function load(player_id, options) {
        var list = new Array();
        for (var i=0; i<options.links.length; i++) {
          options.links[i][options.format] = options.links[i]['uri'];
        }
        myPlaylist[player_id] = new jPlayerPlaylist({
          jPlayer: "#jquery_jplayer_" + player_id,
          cssSelectorAncestor: "#jp_container_" + player_id,
        }, options.links,
         {
          playlistOptions: {
            autoPlay: options.autoplay,
            enableRemoveControls: false,
            displayTime: 0,
          },
          swfPath: "/libraries/jplayer/dist/jplayer/jquery.jplayer.swf",
          supplied: options.format,
          smoothPlayBar: true,
          keyEnabled: true,
          audioFullScreen: true // Allows the audio poster to go full screen via keyboard
        });

        if (options.debug == 1) {
          $("#jplayer_inspector_" + player_id).jPlayerInspector({jPlayer:$("#jquery_jplayer_" + player_id)});
        }
      }




      // Using once() to apply the myCustomBehaviour effect when you want to do just run one function.
      $(context).find('input.myCustomBehavior').once('myCustomBehavior').addClass('processed');

      // Using once() with more complexity.
      $(context).find('input.myCustom').once('mySecondBehavior').each(function () {
        if ($(this).visible()) {
          $(this).css('background', 'green');
        } else {
          $(this).css('background', 'yellow').show();
        }
      });
    }
  };

})(jQuery, Drupal);
