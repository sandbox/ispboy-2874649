(function ($, Drupal) {
  Drupal.behaviors.jplinkStream = {
    attach: function (context, settings) {
      jplink = settings.jplink;
      for (var player_id in jplink) {
        load(player_id, jplink[player_id]);
      }


      function load(player_id, options) {
        var media = options['media'];
        media[options.format] = media['uri'];
        $(context).find("#jquery_jplayer_" + player_id).once().jPlayer({
          ready: function (event) {
            ready = true;
            $(this).jPlayer("setMedia", media);
            if (options.autoplay == 1) {
              $(this).jPlayer('play', 1);
            }
          },
          play: function () {
            $(this).jPlayer("pauseOthers");
          },
          pause: function () {
            $(this).jPlayer("clearMedia");
          },
          error: function (event) {
            if (ready && event.jPlayer.error.type === $.jPlayer.error.URL_NOT_SET) {
              // Setup the media stream again and play it.
              $(this).jPlayer("setMedia", media).jPlayer("play");
            }
          },
          cssSelectorAncestor: '#jp_container_' + player_id,
          swfPath: "/libraries/jplayer/dist/jplayer/jquery.jplayer.swf",
          //supplied: options.format,
          supplied: "mp3, oga",
          preload: "none",
          wmode: "window",
          useStateClassSkin: true,
          'autoBlur': false,
          keyEnabled: true,
          //solution: "flash, html"
        });

        if (options.debug == 1) {
          $("#jplayer_inspector_" + player_id).jPlayerInspector({jPlayer:$("#jquery_jplayer_" + player_id)});
        }
      }




      // Using once() to apply the myCustomBehaviour effect when you want to do just run one function.
      $(context).find('input.myCustomBehavior').once('myCustomBehavior').addClass('processed');

      // Using once() with more complexity.
      $(context).find('input.myCustom').once('mySecondBehavior').each(function () {
        if ($(this).visible()) {
          $(this).css('background', 'green');
        } else {
          $(this).css('background', 'yellow').show();
        }
      });
    }
  };

})(jQuery, Drupal);
