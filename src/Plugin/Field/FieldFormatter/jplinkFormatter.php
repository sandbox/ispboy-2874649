<?php

/**
 * @file
 * Contains \Drupal\jplink\Plugin\Field\FieldFormatter\jplinkFormatter.
 */

namespace Drupal\jplink\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'jplink' formatter.
 *
 * @FieldFormatter(
 *   id = "jplink",
 *   label = @Translation("jPlayer for link Field"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class jplinkFormatter extends FormatterBase {

  private  static  $player_id =  0;

  private function getPlayerId() {
    self::$player_id ++;
    return(self::$player_id);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $settings = $this->getSettings();
    $summary[] = t('Using <em>@template</em> template.', array('@template' => $settings['template']));

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $label = $items->getFieldDefinition()->getLabel();
    $element = array();
    $settings = $this->getSettings();
    $player_id = $this->getPlayerId();
    $links = [];
    foreach ($items as $delta => $item) {
      if (!empty($item->title)) {
        $title = $item->title;
        $uri = $item->uri;
      } else {
        $title = t('Channel @delta', array('@delta' => $delta + 1));
        $uri = $item->uri;
      }
      $links[$delta] = array(
          'title' => $title,
          'uri'   => $uri,
      );
    }

    if (count($links)>0) {
      $settings['media'] = $links[0];
      $settings['links'] = $links;
      $element = array(
        '#theme'  => 'jplink_' . $settings['template'],
        '#links'   => $links,
        '#id'   => $player_id,
        '#label'   => $label,
        '#debug'   => $settings['debug'],
      );

      $element['#attached'] = array(
        'drupalSettings' => array('jplink' => array($player_id =>$settings)),
      );
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'template' => 'default',
      'format'  => 'mp3',
      'limit' => '5',
      'debug'  => '0',
      'autoplay' => '0',
    ) ;
  }


  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['template'] = array(
      '#type' => 'select',
      '#title' => $this->t('Select a template'),
      '#options' => array(
        'default' => $this->t('Default'),
        'button' => $this->t('Single button'),
      ),
      '#default_value' => $this->getSetting('template'),
      '#required' => TRUE,
    );

    $elements['format'] = array(
      '#type' => 'select',
      '#title' => $this->t('Media format'),
      '#options' => array(
        'mp3' => 'mp3',
        'oga' => 'oga',
      ),
      '#default_value' => $this->getSetting('format'),
      '#required' => TRUE,
    );

    $elements['limit'] = array(
      '#title' => t('Limit'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('limit'),
      '#min' => 2,
      '#required' => FALSE,
    );

    $elements['debug'] = array(
      '#title' => t('Debug'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('debug'),
      '#required' => FALSE,
    );

    $elements['autoplay'] = array(
      '#title' => t('Auto play'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('autoplay'),
      '#required' => FALSE,
    );

    return $elements;
  }

}
